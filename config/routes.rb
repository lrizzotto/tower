Rails.application.routes.draw do
	scope :tower, :module => :tower, :as => "tower" do
		get "/:model_name", :controller => "main", :as => "index", :action => "index"
		get "/:model_name/new", :controller => "main", :as => "new", :action => "new"
		get "/:model_name/:id/edit", :controller => "main", :as => "edit", :action => "edit"
		get "/:model_name/:id", :controller => "main", :as => "delete", :action => "delete"
		
		post "/:model_name", :controller => "main", :as => "create", :action => "create"		
		patch "/:model_name", :controller => "main", :as => "update", :action => "update"
		delete "/:model_name/:id", :controller => "main", :as => "delete", :action => "delete"				
	end
end

Tower::Engine.routes.draw do
	root :to => "home#index"
end
