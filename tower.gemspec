$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "tower/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "tower"
  s.version     = Tower::VERSION
  s.authors     = ["Helter Design"]
  s.email       = ["sayHello@helterdign.com.br"]
  s.homepage    = "http://helterdesign.com.br"
  s.summary     = "Tower CMS by Helter"
  s.description = "Made with love in uberlandia <3 <3 <3"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0.beta1"
  s.add_dependency "haml"
  s.add_dependency "turbolinks"

  s.add_development_dependency "sqlite3"
end
