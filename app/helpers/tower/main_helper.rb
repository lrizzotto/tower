module Tower
  module MainHelper
	def path
		"/tower/#{params[:model_name]}"
	end

	def new_path
		"/tower/#{params[:model_name]}/new"
	end

	def show_path id
		"/tower/#{params[:model_name]}/#{id.to_s}"
	end

	def edit_path id
		"/tower/#{params[:model_name]}/#{id.to_s}/edit"
	end

  end
end
