module Tower
  class MainController < ApplicationController
  	before_filter :set_abstract_model
    layout :set_layout

  	def index
  		@objects = @abstract_model.model.all
      @action  = "Index"
  	end

  	def new
  		@object = @abstract_model.model.new      

      if request.headers['X-Requested-With']
        render :layout => false
      end
  	end

    def edit
      @object = @abstract_model.model.find( params[:id] )
    end

  	def create
  		permit_params = Array.new
      model_name    = params[:model_name].singularize.to_sym
      has_ones      = Array.new

  		@abstract_model.columns.each do |column|
          if column.type === 'has_one'
            has_ones.push column.name

          else
  				  permit_params.push column.name.to_sym
          end
    	end

  		@object = @abstract_model.model.new params.require(model_name).permit(permit_params)

      has_ones.each do |has_one|
        blank = true

        params_has_one = params[model_name][has_one].to_a

        params_has_one.each do | value |
          if value[1] != ""
            blank = false
          end 
        end  

        if !blank
          @object.send("#{ has_one.to_s }=", has_one.to_s.classify.constantize.new( params[model_name].require( has_one ).permit! ) )
        end
      end 

      @object.save

      if request.headers['X-Requested-With']
        object_json = Array.new
        
        object_json.push @object.id
        object_json.push @object.title

        render :json => object_json

      else  
        redirect_to "/tower/#{params[:model_name]}"
  	  end

    end

    def delete
      @object = @abstract_model.model.find params[:id]

      @object.destroy

      redirect_to :back
    end

  	private

  	def set_abstract_model
  		@abstract_model = Tower::AbstractModel.new params[:model_name]          
  	end

    def set_layout
        "tower/application"
    end

  end
end
