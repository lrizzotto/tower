//= require jquery
//= require tower/redactor
//= require jquery_ujs
//= require turbolinks


	$(document).ready(function()
	{
		document.addEventListener("page:load", function( e )
		{
			init();
		});		

		init();
	});

	function init()
	{
		components();
		modal_create_object();
		$('textarea').redactor();

		if( document.getElementsByTagName("canvas").length > 0 )
			graphs();				
	}

	function modal_create_object()
	{
		$('a.open-create-object').click(function()
		{
			var t = $(this)

			open_modal('.modal.create-object');

			$('.modal.create-object .modal-content').load( $(this).attr('href'), function()
			{
				$('.modal.create-object .form-actions .btn').unbind('click');
				$('.modal.create-object .form-actions .btn:first').click(function()
				{	
					close_modal();
					$('.modal.create-object .modal-content').html('');
				});				
			
				$('.modal.create-object .form-actions .btn:last').click(function()
				{	
					$.post( $('.modal.create-object form').attr('action'), $('.modal.create-object form').serialize(), function( data )
					{
						t.prev('select').children('option[selected]').removeAttr('selected');
						t.prev('select').append('<option value="'+ data[0] +'" selected="selected">'+ data[1] +'</option>');
					
						close_modal();
						$('.modal.create-object .modal-content').html('');
					});

					return false;
				});				
			});

			return false;
		});		
	}

	function components()
	{
		$(".btn-group").click(function()
		{
			$(this).children(".dropdown-menu").addClass("active").fadeIn(200, function()
			{
				$(document).click(function()
				{  
					$(".dropdown-menu").removeClass("active").fadeOut(200);
					$(document).unbind("click");
				});

			});
		});

		$(".image-field").bind("change", fire_file_select);		
	}

	function fire_file_select( evt )
	{
	 	var file 		= evt.target.files;		
		var reader 		= new FileReader();		
		var field_value = $(this).val();
		var $this  		= $(this);

		if( field_value == "" )
			return false;

		field_value 	= field_value.split(/\\|\//)
		field_value 	= field_value[ field_value.length - 1 ]

        $this.parent("span").children("span.text").text( field_value )

 		reader.onload = (function(theFile)
 		{
        	return function(e)
        	{
        		$this.parent("span").children("span.image").children("img").remove();
        		$this.parent("span").children("span.image").append("<img src='"+e.target.result+"' />")

        		$this.parent("span").children("span.bt.bt-upload").hide();
        		$this.parent("span").children("span.bt.bt-remove").show();
        	};

     	})(file[0]);

      	reader.readAsDataURL(file[0]);
	
      	$(this).bind("click", function()
      	{
      		if( $this.parent("span").children("span.text").text() != "" )
      		{
      			open_modal(".modal-alert.modal-remove-image");

      			$(".modal-alert.modal-remove-image .buttons a").unbind("click");

      			$(".modal-alert.modal-remove-image .buttons a:first").bind("click", function(){  close_modal(); return false; });
				$(".modal-alert.modal-remove-image .buttons a:last").bind("click", function()
				{  
					close_modal(); 

		    		$this.parent("span").children("span.bt.bt-upload").show();
		    		$this.parent("span").children("span.bt.bt-remove").hide();
		    		$this.parent("span").children("span.text").text("");
					$this.parent("span").children("span.image").children("img").remove();
					$this.val("");

					$(".modal-alert.modal-remove-image .buttons a").unbind("click");					
					$this.unbind("click");

					return false; 
				});  			
      		}

	      	return false;         		
      	})
	}

	function open_modal( modal )
	{	
		$(modal).fadeIn(300);
		$(".bkg-modal").fadeIn(300);
	}

	function close_modal()
	{
		$(".modal:visible").fadeOut(300);
		$(".modal-alert:visible").fadeOut(300);
		$(".bkg-modal").fadeOut(300);
	}





// HOME

	var points,ctx, w, h, pointsAnimate;

	function graphs()
	{
		points = new Array();

		ctx = document.getElementsByTagName("canvas")[0].getContext("2d");
		w   = document.getElementsByTagName("canvas")[0].width;
		h   = document.getElementsByTagName("canvas")[0].height;
	
		points.push( new Vect2f( 0 + 8, h ) );
		points.push( new Vect2f( 109, h ) );
		points.push( new Vect2f( 218, h ) );
		points.push( new Vect2f( 325, h ) );
		points.push( new Vect2f( 439, h ) );
		points.push( new Vect2f( 547, h ) );
		points.push( new Vect2f( 655, h ) );
		points.push( new Vect2f( 762, h ) );
		points.push( new Vect2f( 872, h ) );
		points.push( new Vect2f( w -8, h ) );

		pointsAnimate = new Array();
		pointsAnimate[0] = h -8;
		pointsAnimate[1] = 85;
		pointsAnimate[2] = 44;
		pointsAnimate[3] = 61;
		pointsAnimate[4] = 116;
		pointsAnimate[5] = 54;
		pointsAnimate[6] = 107;
		pointsAnimate[7] = 105;
		pointsAnimate[8] = 61;
		pointsAnimate[9] = h - 8;

		render();

		window.setInterval( render, 5 );
	}

	function render()
	{	
		ctx.clearRect(0, 0, w , h );

		grid();

		ctx.lineJoin = 'round';
	    ctx.lineCap = 'round';
		ctx.lineWidth = 3;
		ctx.strokeStyle = "#48afaa";


		ctx.beginPath();
		for( var i = 0; i < points.length; i++ )
		{
			points[i].y += ( pointsAnimate[i] - points[i].y) / (5 + ( i * 10 ) )

			if( i != 0 )
				ctx.lineTo( points[i].x, points[i].y )

			else
				ctx.moveTo( points[i].x, points[i].y )				
		}

		ctx.stroke();
		ctx.closePath();
	
		for( var i = 0; i < points.length; i++ )
		{
			ctx.beginPath();
			ctx.arc(points[i].x, points[i].y, 8, 0, Math.PI*2, false);
			ctx.fillStyle = "#30363f";
			ctx.fill();

			ctx.beginPath();
			ctx.arc(points[i].x, points[i].y, 3, 0, Math.PI*2, false);
			ctx.fillStyle = "#48afaa";
			ctx.fill();
		}
		ctx.closePath();
	}


	function grid()
	{
		var gridPos = 0;

		ctx.lineWidth = 1;

		for( var i = 0; i < 10; i++)
		{	
			ctx.beginPath();
			ctx.moveTo(gridPos, 0 )
			ctx.lineTo(gridPos, h )
			ctx.closePath();
			ctx.strokeStyle = "#40464e";
			ctx.stroke();
			
			gridPos += w/9; 
		}

		gridPos = 0;

		for( var i = 0; i < 5; i++)
		{	
			ctx.beginPath();
			ctx.moveTo(0, gridPos )
			ctx.lineTo(w, gridPos )
			ctx.closePath();
			ctx.strokeStyle = "#40464e";
			ctx.lineWidth = 0.5;
			ctx.stroke();
			
			gridPos += h/4; 
		}

	}


	var Vect2f = function( _x, _y )
	{
		this.x = _x;
		this.y = _y;
	}	