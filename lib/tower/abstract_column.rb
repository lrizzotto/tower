module Tower
	class AbstractColumn
		attr_accessor :name, :type, :association_model, :show_in_table, :show_in_form, :association_from

		def initialize name, type, show_in_table, show_in_form, association_model = nil, association_from = nil
			@name = name
			@type = type 
			@association_model 	= association_model
			@show_in_table 		= show_in_table
			@show_in_form  		= show_in_form
			@association_from   = association_from
		end
	end	
end
