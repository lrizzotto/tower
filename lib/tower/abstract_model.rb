require 'tower/abstract_column'

module Tower
	class AbstractModel
		attr_accessor :model		

		public
		def initialize model
			@model = model.classify.constantize
		end

		def columns
			@columns = Array.new
			default_remove_columns_table = [ "description", "content", "created_at", "reference" ]
			default_remove_columns_form  = [ "id", "trash_id", "user_id", "created_at", "updated_at" ]

			@model.columns.each do |model_column|
				show_in_table = false
				show_in_form  = false

				if default_remove_columns_table.grep( model_column.name ).length === 0
					show_in_table = true
				end	

				if default_remove_columns_form.grep( model_column.name ).length === 0
					show_in_form = true
				end	

				@columns.push AbstractColumn.new model_column.name, model_column.type, show_in_table, show_in_form				
			end

			if !@model.attachment_definitions.nil?
				set_paperclip @columns
			end

			set_assosiations @columns
		end

		def singular_name
			@model.name.to_s.humanize
		end	

		def plural_name
			@model.name.to_s.pluralize.humanize
		end

		private

		def set_assosiations columns
			@model.reflect_on_all_associations.each do | association |
				
				if association.macro.to_s == "belongs_to"
					columns.each do |column|
						if( column.name == "#{association.name.to_s}_id" )
							column.type 				= "belongs_to"
							column.association_model 	= association.name.to_s.classify.constantize					
						end
					end
				else
					columns.push AbstractColumn.new association.name, association.macro[0..-1], association.name.to_s.classify.constantize, true, true, @model.name.downcase
				end	
			end	

			return columns
		end

		def set_paperclip columns
			has_attached 		= @model.attachment_definitions.to_a

			has_attached.each do |att|
				columns_processed   = Array.new
				attached 			= att[0]

				columns.each do |column|
					name = column.name

					if(name != "#{attached}_file_name" && name != "#{attached}_content_type" && name != "#{attached}_file_size" && name != "#{attached}_updated_at")
						columns_processed.push column					
					end
				end

				columns_processed.push AbstractColumn.new "#{attached}", "paperclip", false, true
				columns.replace columns_processed
			end	
		end
	end
end	